const mongo = require('then-mongo')
const config = require('../config.json')

const db = mongo(config.mongoconnection,
  ['drink_rounds_users',
    'drink_rounds_sessions',
    'drink_rounds_rounds',
    'drink_rounds_activity'
  ])

module.exports = db