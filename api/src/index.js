const express = require('express')
const bodyParser = require('body-parser')
const validate = require('express-validation')
const session = require('express-session')
const MongoDBStore = require('connect-mongodb-session')(session)
const assert = require('assert')
const app = express()
const config = require('../config.json')

// Validation include
const validation = require('./services/validation.js')

// General include
const general = require('./services/general.js')

// Require routes
const user = require('./webroutes/user.js')

app.listen(3001, () => { //Port to be set by core in future
  console.log(`Started on 3001`)
})

// Mongo DB Store Init
const store = new MongoDBStore(
  {
    uri: config.mongoconnection,
    collection: 'drink_rounds_cookies'
  })

store.on('error', (error) => {
  assert.ifError(error)
  assert.ok(false)
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use((req, res, next) => {
  // res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Origin', 'http://localhost:8080')
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.set('Content-Type', 'application/json')
  next()
})

// Express Session Init
app.use(session({
  name: 'drink-s',
  secret: '89rBTi80qsEsMYqzYPA6SyKFmd3sqT6NFI9490r3PrpIfx0mQMZSzuLvOPDu3uUMK8wKyk3zJmvEwi3JfSR2EcHI5STdz5pQRohbIVHLIch1Kqk1Vm4L3NuxIT3lbSkL',
  store: store,
  resave: true,
  saveUninitialized: false,
  cookie: { expires: 2629746000, sameSite: true, httpOnly: true }
}))

// Login (Done)
app.post('/user/login', validate(validation.user.login), (req, res) => {
  console.log('Request received')
  user.login(req.body)
    .then(result => {
      req.session.token = result.sessionToken
      delete result['sessionToken']
      res.send(general.createResponse('SUCCESS', result))
    })
    .catch(err => res.send(general.createResponse('ERROR', err)))
})
// Session check (Done)
app.get('/user/session', validate(validation.user.session), (req, res) => {
  user.session(req.session.token)
    .then(result => res.send(general.createResponse('SUCCESS', result)))
    .catch(err => res.send(general.createResponse('ERROR', err)))
})
// Sign up / Verification (Done)
app.post('/user/signup', validate(validation.user.signup), (req, res) => {
  user.signup(req.body)
    .then(result => res.send(general.createResponse('SUCCESS', result)))
    .catch(err => res.send(general.createResponse('ERROR', err)))
})
// Logout
app.post('/user/logout', validate(validation.user.logout), (req, res) => {
  user.logout(req.session.token)
    .then(() => {
      req.session.destroy(err => {
        if (!err)
          res.send(general.createResponse('SUCCESS', true))
        else
          throw 'SESSION_DELETE_FAILED'
      })
    })
    .catch(err => res.send(general.createResponse('ERROR', err)))

})
app.post('/user/addFriend', validate(validation.user.addFriend), (req, res) => {
  user.addFriend(req.session.token, req.body)
    .then(result => res.send(general.createResponse('SUCCESS', result)))
    .catch(err => res.send(general.createResponse('ERROR', err)))
})
app.post('/user/removeFriend', validate(validation.user.removeFriend), (req, res) => {
  user.removeFriend(req.session.token, req.body)
    .then(result => res.send(general.createResponse('SUCCESS', result)))
    .catch(err => res.send(general.createResponse('ERROR', err)))
})

// Forgot password
// Reset password

// Get user rounds/history
// Start new round
app.post('/rounds/new', validate(validation.rounds.new), (req, res) => {
  rounds.new(req.session.token, req.body)
    .then(result => res.send(general.createResponse('SUCCESS', result)))
    .catch(err => res.send(general.createResponse('ERROR', err)))
})
// Edit round
// Finish round
// Delete session

// Add activity to round
// Edit activity
// Delete activity

