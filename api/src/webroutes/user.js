const userServices = require('../services/user.js')

const user = {
  login: (request) => {
    return new Promise((resolve, reject) => {
      userServices.checkLogin(request)
        .then(userData => userServices.createUserSession(userData))
        .then(result => {
          delete result['_id']
          delete result['password']
          resolve(result)
        })
        .catch(err => reject(err))
    })
  },
  session: (sessionToken) => {
    return new Promise((resolve, reject) => {
      userServices.getUserID(sessionToken)
        .then(userID => userServices.getUserInfo(userID))
        .then(userInfo => resolve(userInfo))
        .catch(err => reject(err))
    })
  },
  signup: (request) => {
    return new Promise((resolve, reject) => {
      userServices.checkExistingUser(request)
        .then(() => userServices.insertUser(request))
        .then(result => resolve(result))
        .catch(err => {
          console.log(err)
          reject(err)
        })
    })
  },
  logout: (sessionToken) => {
    return new Promise((resolve, reject) => {
      userServices.setSessionInactive(sessionToken)
        .then(result => resolve())
        .catch(err => reject(err))
    })
  },
  addFriend: (sessionToken, request) => {
    return new Promise((resolve, reject) => {
      userServices.getUserID(sessionToken)
        .then(userID => userServices.addFriend(userID, request.userToAdd))
        .then(result => resolve(result))
        .catch(err => reject(err))
    })
  },
  removeFriend: (sessionToken, request) => {
    return new Promise((resolve, reject) => {
      userServices.getUserID(sessionToken)
        .then(userID => userServices.removeFriend(userID, request.userToRemove))
        .then(result => resolve(result))
        .catch(err => reject(err))
    })
  }
}

module.exports = user