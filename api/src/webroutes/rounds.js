const roundsServices = require('../services/rounds.js')
const userServices = require('../services/user.js')

const rounds = {
  new: (sessionToken, newRoundData) => {
    return new Promise((resolve, reject) => {
      userServices.getUserID(sessionToken)
        .then(userID => {
          newRoundData.otherUser.unshift(userID)
          return
        })
        .then(() => roundsServices.insertRound(newRoundData))
        .then(result => resolve(result))
        .catch(err => reject(err))
    })
  }
}

module.exports = rounds