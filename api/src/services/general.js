const general = {
  createResponse: (result, message) => {
    return JSON.stringify({
      'state': result,
      'message': message
    })
  }
}

module.exports = general