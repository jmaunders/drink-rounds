const Joi = require('joi')

const validation = {
  user: {
    login: {
      body: {
        email: Joi.string().email().required(),
        password: Joi.string().required()
      }
    },
    signup: {
      body: {
        nickName: Joi.string().alphanum().required(),
        firstName: Joi.string().alphanum().required(),
        lastName: Joi.string().alphanum().required(),
        email: Joi.string().email().required(),
        mobile: Joi.string().regex(/^(\+\d{1,3}[- ]?)?\d{10}$/),
        password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/)
      }
    },
    session: {
      
    },
    logout: {
      
    },
    forgot: {
      email: Joi.string().required()
    },
    reset: {
      email: Joi.string().email().required(),
      code: Joi.string().required()
    },
    addFriend: {
      body: {
        userToAdd: Joi.string().required()
      }
    },
    removeFriend: {
      body: {
        userToRemove: Joi.string().required()
      }
    }
  },
  rounds: {
    new: {
      body: {
        name: Joi.string().required(),
        date: Joi.date().required(),
        location: Joi.string().required(),
        otherUsers: Joi.array().required()
      }
    }
  }
}

module.exports = validation