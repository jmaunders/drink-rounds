const db = require('../mongo.js')

const roundsServices = {
  insertRound: (roundData) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_rounds.insert({
        name: roundData.name,
        date: roundData.firstName,
        lastName: roundData.location,
        users: roundData.otherUsers
      })
        .then(() => resolve(true))
        .catch(err => reject(err))
    })
  }
}

module.exports = roundsServices