const db = require('../mongo.js')
const encryption = require('./encryption.js')
const Chance = require('chance')
const chance = new Chance()

const userServices = {
  addFriend: (userID, userToAdd) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.update({
        _id: userID
      }, {
        $push: { friends: userToAdd }
        })
        .then(() => resolve(true))
        .catch(err => reject(err))
    })
  },
  checkExistingUser: (userData) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.find({
        $or: [
          { nickName: userData.nickName },
          { email: userData.email },
          { mobile: userData.mobile }
        ]
      })
        .then(result => {
          if (result.length > 0)
            reject('USER_DATA_IN_USE')
          else
            resolve(true)
        })
        .catch(err => reject(err))
    })
  },
  checkLogin: (userData) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.find({
        email: userData.email
      })
        .then(result => {
          if (result.length === 1) {
            if (encryption.bcrypt.compare(userData.password, result[0].password)) {
              resolve(result[0])
            } else
              reject('NO_MATCH')
          } else
            reject('USER_NOT_FOUND')
        })
        .catch(err => reject(err))
    })
  },
  createUserSession: (userData) => {
    return new Promise((resolve, reject) => {
      let newToken = chance.guid()
      db.drink_rounds_sessions.insert({
        _id: newToken,
        userID: userData._id,
        status: 1
      })
        .then(() => {
          userData.sessionToken = newToken
          resolve(userData)
        })
        .catch(err => reject(err))
    })
  },
  getUserID: (sessionToken) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_sessions.findOne({
        _id: sessionToken
      })
        .then(result => {
          resolve(result.userID)
        })
        .catch(err => reject(err))
    })
  },
  getUserInfo: (userID) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.findOne({
        _id: userID
      })
        .then(result => {
          delete result['_id']
          delete result['password']
          resolve(result)
        })
        .catch(err => reject(err))
    })
  },
  insertUser: (userData) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.insert({
        nickName: userData.nickName,
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        mobile: userData.mobile,
        password: encryption.bcrypt.hash(userData.password),
        friends: []
      })
        .then(() => resolve(true))
        .catch(err => reject(err))
    })
  },
  removeFriend: (userID, userToRemove) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_users.update({
        _id: userID
      }, {
        $pull: { friends: userToRemove }
        })
        .then(() => resolve(true))
        .catch(err => reject(err))
    })
  },
  setSessionInactive: (sessionToken) => {
    return new Promise((resolve, reject) => {
      db.drink_rounds_sessions.update({
        "_id": sessionToken,
      }, {
          $set: { "status": 0 }
        })
        .then(() => resolve(true))
        .catch(err => reject(err))
    })
  }
}

module.exports = userServices