const bcrypt = require('bcrypt-node')

let encryption = {

  bcrypt: { // one way encryption (hashing), use for passwords and info you'd not want to be decrypted

    hash: (plaintext) => { // one way hash
      return bcrypt.hashSync(plaintext)
    },

    compare: (plaintext, hash) => { // compare an plaintext with an existing hash
      return bcrypt.compareSync(plaintext, hash)
    }

  }

}

module.exports = encryption