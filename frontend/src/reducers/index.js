import { combineReducers } from 'redux'
import LoggedInUser from './reducer_LoggedInUser'

const rootReducer = combineReducers({
  LoggedInUser: LoggedInUser
})

export default rootReducer