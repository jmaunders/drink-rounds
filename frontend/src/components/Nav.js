import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer'
import { MenuItem } from 'material-ui'

import { connect } from 'react-redux'

class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = { open: false }
    console.log(this.props.LoggedInUser)
  }

  handleDrawerToggle = () => this.setState({ open: !this.state.open })
  handleDrawerClose = () => this.setState({ open: false })

  mapUserDetails() {
    console.log(this.props.LoggedInUser)
    if (this.props.LoggedInUser) {
      return Object.keys(this.props.LoggedInUser).map((key) => {
        return (
          <li key={key}>{this.props.LoggedInUser[key]}</li>
        )
      })
    } else {
      return
    }
  }

  render() {
    return (
      <div>
        <AppBar
          title='App Navbar'
          className='navbar-title'
          onLeftIconButtonClick={this.handleDrawerToggle}
        />
        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({ open })}
        >
          <MenuItem containerElement={<Link to='/login' />} onClick={this.handleDrawerClose}>Login</MenuItem>
          <MenuItem containerElement={<Link to='/register' />} onClick={this.handleCloseClose}>Register</MenuItem>
        </Drawer>
        <ul>
          {this.mapUserDetails()}
        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    LoggedInUser: state.LoggedInUser
  }
}

export default connect(mapStateToProps)(Nav)