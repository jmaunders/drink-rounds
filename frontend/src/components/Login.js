import React, { Component } from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import { connect } from 'react-redux'
import { logIn } from '../actions/index'
import { bindActionCreators } from 'redux'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = { emailValue: '', passwordValue: '' }
  }

  handleEmailChange = (event) => { this.setState({ emailValue: event.target.value }) }
  handlePasswordChange = (event) => { this.setState({ passwordValue: event.target.value }) }

  sendLoginForm = (event) => {
    event.preventDefault()
    fetch('http://localhost:3001/user/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify({
        email: this.state.emailValue,
        password: this.state.passwordValue
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.props.logIn(result.message)
        },
        (error) => {
          error
        }
      )
  }

  render() {
    return (
      <div>
        <form onSubmit={this.sendLoginForm} autoComplete='off'>
          <TextField hintText='Email'>
            <input type='text' name='email' value={this.state.emailValue} onChange={this.handleEmailChange}></input>
          </TextField>
          <br />
          <TextField hintText='Password'>
          <input type='password' name='password' value={this.state.passwordValue} onChange={this.handlePasswordChange}></input>
          </TextField>
          <br />
          <RaisedButton type="submit" label="Login" primary={true} />
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    LoggedInUser: state.LoggedInUser
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ logIn: logIn }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)