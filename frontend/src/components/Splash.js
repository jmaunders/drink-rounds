import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'

class Splash extends Component {
  render() {
    return (
      <div>
        <h1>Splash Page</h1>
        <h2>Drink Rounds</h2>
        <RaisedButton containerElement={<Link to='/home' />} label='Enter' primary={true} />
      </div>
    )
  }
}

export default Splash