import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'

import Splash from '../components/Splash'
import Home from '../components/Home'
import Login from '../components/Login'
import Register from '../components/Register'

class Main extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/' component={Splash} />
          <Route exact path='/home' component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' component={Register} />
          {/* <Route exact path='/rounds/view' component={ViewRounds} />
          <Route exact path='/rounds/add' component={AddRound} />
          <Route exact path='/rounds/view/:roundId' component={ViewRound} />
          <Route exact path='/rounds/edit/:roundId' component={EditRound} />
          <Route exact path='/rounds/:roundId/activity/add' component={AddActivity} />
          <Route exact path='/rounds/:roundId/activity/view/:activityId' component={ViewActivity} />
          <Route exact path='/rounds/:roundId/activity/edit/:activityId' component={EditActivity} />
          <Route exact path='/friends' component={Friends} /> */}
        </Switch>
      </div>
    )
  }
}

export default Main
