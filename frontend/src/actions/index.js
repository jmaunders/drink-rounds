export function logIn(userInfo) {
  return {
    type: 'LOG_IN',
    payload: userInfo
  }
}